<?php

/**
 * @file
 * Admin form callbacks for AB Tasty module
 */

/**
 * AB Tasty settings form
 */
function ab_tasty_official_admin_settings_form($form_state) {
  $form['ab_tasty_official_account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),

  );

    $form['ab_tasty_official_account'] ['ab_tasty_official_account_number'] = array(
    '#type' => 'textfield',
    '#title' => t('AB Tasty account ID'),
	'#required' => TRUE,
	'#default_value' => variable_get('ab_tasty_official_account_number', ''),
	'#description' => t('Log in on abtasty.com and find your unique account ID on the Dashboard by clicking on the top right button AB Tasty Tag. This ID does not change. Enter it once for all !'),
  );

  $form['ab_tasty_official_account']['ab_tasty_official_account_tracking'] = array(
    '#type' => 'checkbox',
    '#title' => t('Track transactions'),
    '#default_value' => variable_get('ab_tasty_official_account_tracking', 1),
    '#description' => t('Enable checkout tracking to compare variations on revenue. It automatically adds a transaction goal on all you coming tests.'),
  );


  return system_settings_form($form);

}